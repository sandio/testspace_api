import pytest
import requests_mock
from api import space_api
from api import testspace_rest_query
import json


def load_request_url(self):
    with open("pytest/spaces.txt") as myfile:
        self.request_text = "".join(line.rstrip() for line in myfile)

    self.request_json = json.loads(self.request_text)
    self.request_url = 'https://ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS:@abc.testspace.com/api/projects/proj_x/spaces'


class TestSpacespaceApi():
    @pytest.fixture(autouse=True)
    def setup_class(self):
        load_request_url(self)

    def test_space_list(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            response = testspace_rest_query.spaces_get('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                       'abc',
                                                       'proj_x',
                                                       enterprise=None)

        results_sets = space_api.TestspaceSpaceApi(response.json())
        space_list = results_sets.get_space_list()
        assert "auth_by_jwt" in space_list

    def test_space_name_by_id(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            response = testspace_rest_query.spaces_get('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                       'abc',
                                                       'proj_x',
                                                       enterprise=None)
        results_sets = space_api.TestspaceSpaceApi(response.json())
        assert 'account_tab_for_ent' == results_sets.get_space_name_by_id(1948)

    def test_space_name_by_invalid_id(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            response = testspace_rest_query.spaces_get('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                       'abc',
                                                       'proj_x',
                                                       enterprise=None)
        results_sets = space_api.TestspaceSpaceApi(response.json())
        assert results_sets.get_space_name_by_id(100) is None

    def test_space_id_by_name(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            response = testspace_rest_query.spaces_get('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                       'abc',
                                                       'proj_x',
                                                       enterprise=None)
        results_sets = space_api.TestspaceSpaceApi(response.json())
        assert 2219 == results_sets.get_space_id_by_name('add_strenght_indicator_in_insights')

    def test_space_id_by_invalid_name(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            response = testspace_rest_query.spaces_get('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                       'abc',
                                                       'proj_x',
                                                       enterprise=None)
        results_sets = space_api.TestspaceSpaceApi(response.json())
        assert results_sets.get_space_id_by_name('invalid_space_name') is None

    def test_create_space_by_name(self):
        with requests_mock.Mocker() as mock:
            space = self.request_json[0]
            payload = {}
            payload['name'] = space['name']
            payload['description'] = space['description']

            mock.post(self.request_url, status_code=201, json=payload)
            response = testspace_rest_query.spaces_post('',
                                                        'ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                        'abc',
                                                        'proj_x',
                                                        enterprise=None)
        assert response.status_code == 201
        response_json = json.loads(response.text)
        assert response_json['name'] == payload['name']

    def test_create_space_by_name_existing(self):
        with requests_mock.Mocker() as mock:
            space = self.request_json[0]
            payload = {}
            payload['name'] = space['name']
            payload['description'] = space['description']

            existing_space_json = json.loads(
                '{"message":"Name has already been taken by an existing space (validation is case insensitive)"}')
            mock.post(self.request_url, status_code=422, json=existing_space_json)
            response = testspace_rest_query.spaces_post('',
                                                        'ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                        'abc',
                                                        'proj_x',
                                                        enterprise=None)

        assert response.status_code == 422
        response_json = json.loads(response.text)
        assert response_json == existing_space_json
