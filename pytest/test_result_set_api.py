import requests_mock
from api import testspace_rest_query
from api import result_set_api
import json
import pytest


def load_request_url(self):
    with open("pytest/spaces_result_set.txt") as myfile:
        space_results_string = "".join(line.rstrip() for line in myfile)

    space_results_json = json.loads(space_results_string)

    self.request_text = json.dumps(space_results_json[0])
    self.request_url = 'https://testspace.com/api/spaces/508/result_sets/8712'


class TestResultSetApi:
    @pytest.fixture(autouse=True)
    def setup_class(self):
        load_request_url(self)

    def test_result_id(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            result_response = testspace_rest_query.testspace_get(self.request_url)
        if result_response.status_code is 200:
            result_set = result_set_api.TestspaceResultSetApi(result_response.json())
            assert result_set.get_result_id() == int(self.request_url.split('/')[-1])

    def test_number_passing(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            result_response = testspace_rest_query.testspace_get(self.request_url)
        result_set = result_set_api.TestspaceResultSetApi(result_response.json())
        assert result_set.get_passing_tests() == 42

    def test_number_failing(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            result_response = testspace_rest_query.testspace_get(self.request_url)
        result_set = result_set_api.TestspaceResultSetApi(result_response.json())
        assert result_set.get_failing_tests() == 0

    def test_health_state_healthy(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            result_response = testspace_rest_query.testspace_get(self.request_url)
        result_set = result_set_api.TestspaceResultSetApi(result_response.json())
        assert result_set.get_health_state() == 'success'

    def test_health_description_healthy(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            result_response = testspace_rest_query.testspace_get(self.request_url)
        result_set = result_set_api.TestspaceResultSetApi(result_response.json())
        assert 'Healthy' in result_set.get_health_description()

    def test_number_excluded_test_cases(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            result_response = testspace_rest_query.testspace_get(self.request_url)
        result_set = result_set_api.TestspaceResultSetApi(result_response.json())
        assert result_set.get_number_excluded_test_cases() == 3

    def test_list_excluded_test_cases(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            result_response = testspace_rest_query.testspace_get(self.request_url)
        result_set = result_set_api.TestspaceResultSetApi(result_response.json())
        assert len(result_set.get_excluded_tests()) == 3
