class TestspaceResultSetApi:
    def __init__(self, result_json):
        self.result_json = result_json

    def get_result_id(self):
        return self.result_json['id']

    def get_passing_tests(self):
        return self.result_json['case_counts'][0]

    def get_failing_tests(self):
        return self.result_json['case_counts'][1]

    def get_health_state(self):
        return self.result_json['health']['state']

    def get_health_description(self):
        return self.result_json['health']['description']

    def get_excluded_tests(self):
        excluded_tests = {}
        exclusions = self.result_json['excluded_test_cases']
        for test_suite, ts_values in exclusions.items():
            for test_case, tc_values in ts_values.items():
                test_name = test_suite + '.' + test_case
                excluded_tests[test_name] = tc_values['message']
        return excluded_tests

    def get_number_excluded_test_cases(self):
        exclusions = TestspaceResultSetApi.get_excluded_tests(self)
        return len(exclusions)
