import requests


def testspace_get(url):
    try:
        response = requests.get(url)
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        raise requests.exceptions.HTTPError
    else:
        return response


def testspace_post(url, payload):
    try:
        response = requests.post(url, json=payload)
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        raise requests.exceptions.HTTPError
    else:
        return response


def space_result_sets(token, organization, project, space, enterprise=None):
    url = __results_set_url(token, organization, project, space, enterprise)
    return testspace_get(url)


def space_result_set(token, organization, project, space, set_number, enterprise=None):
    url = __results_set_url(token, organization, project, space, enterprise)
    url = url + '/' + str(set_number)
    return testspace_get(url)


def projects_get(token, organization, enterprise=None):
    url = "%s/projects" % (__testspace_api_url(token, organization, enterprise))
    return testspace_get(url)


def projects_post(payload, token, organization, enterprise=None):
    url = "%s/projects" % (__testspace_api_url(token, organization, enterprise))
    return testspace_post(url, payload)


def spaces_get(token, organization, project, enterprise=None):
    url = "%s/spaces" % (__project_url(token, organization, project, enterprise))
    return testspace_get(url)


def spaces_post(payload, token, organization, project, enterprise=None):
    url = "%s/spaces" % (__project_url(token, organization, project, enterprise))
    return testspace_post(url, payload)


def __testspace_api_url(token, organization, enterprise):
    if enterprise is not None:
        url = "https://%s:@%s.testspace.%s.com/api" % (token, organization, enterprise)
    else:
        url = "https://%s:@%s.testspace.com/api" % (token, organization)
    return url


def __project_url(token, organization, project, enterprise):
    url = "%s/projects/%s" % (__testspace_api_url(token, organization, enterprise), project)
    return url


def __space_set_url(token, organization, project, space, enterprise):
    url = "%s/spaces/%s" % (__project_url(token, organization, project, enterprise), space)
    return url


def __results_set_url(token, organization, project, space, enterprise):
    url = "%s/result_sets" % __space_set_url(token, organization, project, space, enterprise)
    return url
