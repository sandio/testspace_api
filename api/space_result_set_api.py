class TestspaceSpaceResultSetApi:
    def __init__(self, result_list_json):
        self.result_list_json = result_list_json
        TestspaceSpaceResultSetApi.__sort_list_by_end_time(self.result_list_json)

    def get_number_results(self):
        return len(self.result_list_json)

    def get_list_result_names(self):
        result_name_list = []
        for result_set in self.result_list_json:
            result_name_list.append(result_set['name'])
        return result_name_list

    def get_result_by_name(self, name):
        for result_set in self.result_list_json:
            if result_set['name'] == name:
                return result_set
        return None

    def get_last_updated(self):
        return self.result_list_json[0]

    @staticmethod
    def __sort_list_by_end_time(result_list_json):
        changed = True
        while changed:
            changed = False
            for i in range(len(result_list_json) - 1):
                if result_list_json[i]['updated_at'] < result_list_json[i+1]['updated_at']:
                    result_list_json[i], result_list_json[i+1] = result_list_json[i + 1], result_list_json[i]
                    changed = True
