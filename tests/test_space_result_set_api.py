from unittest import TestCase
import requests_mock
from api import space_result_set_api
from api import testspace_rest_query
import json


with open("spaces_result_set.txt") as myfile:
    request_text = "".join(line.rstrip() for line in myfile)

request_json = json.loads(request_text)
request_url = 'https://testspace.com/api/spaces/829/result_sets'


class TestSpaceResultSetApi(TestCase):
    @requests_mock.mock()
    def test_number_result_sets(self, mock):
        mock.get(request_url, text=request_text)
        result_response = testspace_rest_query.testspace_get(request_url)
        results_sets = space_result_set_api.TestspaceSpaceResultSetApi(result_response.json())
        self.assertEqual(len(request_json), results_sets.get_number_results())

    @requests_mock.mock()
    def test__result_set_name_list(self, mock):
        mock.get(request_url, text=request_text)
        result_response = testspace_rest_query.testspace_get(request_url)
        results_sets = space_result_set_api.TestspaceSpaceResultSetApi(result_response.json())
        self.assertEqual(len(request_json), len(results_sets.get_list_result_names()))

    @requests_mock.mock()
    def test__result_set_by_name(self, mock):
        mock.get(request_url, text=request_text)
        result_response = testspace_rest_query.testspace_get(request_url)
        results_sets = space_result_set_api.TestspaceSpaceResultSetApi(result_response.json())
        results_set = results_sets.get_result_by_name("c9.Build")
        self.assertEqual(7355, results_set['id'])

    @requests_mock.mock()
    def test__result_set_by_name_invalid(self, mock):
        mock.get(request_url, text=request_text)
        result_response = testspace_rest_query.testspace_get(request_url)
        results_sets = space_result_set_api.TestspaceSpaceResultSetApi(result_response.json())
        results_set = results_sets.get_result_by_name("build")
        self.assertEqual(None, results_set)

    @requests_mock.mock()
    def test__result_set_last_updated(self, mock):
        mock.get(request_url, text=request_text)
        result_response = testspace_rest_query.testspace_get(request_url)
        results_sets = space_result_set_api.TestspaceSpaceResultSetApi(result_response.json())
        results_set = results_sets.get_last_updated()
        self.assertEqual(8722, results_set['id'])
