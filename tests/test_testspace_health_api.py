from unittest import TestCase
import requests_mock
from api import result_set_api
from api import testspace_rest_query
import json

with open("spaces_result_set.txt") as myfile:
    space_results_string = "".join(line.rstrip() for line in myfile)

space_results_json = json.loads(space_results_string)

request_text = json.dumps(space_results_json[0])
request_url = 'https://testspace.com/api/spaces/508/result_sets/8712'


class TestResultRequest(TestCase):
    @requests_mock.mock()
    def test_request_200(self, mock):
        mock.get(request_url, text=request_text)
        response = testspace_rest_query.testspace_get(request_url)
        self.assertEqual(response.status_code, 200)

    def test_request_request_404(self):
        response = testspace_rest_query.testspace_get(request_url)
        self.assertEqual(response.status_code, 404)

    @requests_mock.mock()
    def test_space_result_sets(self, mock):
        url = 'https://ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS:@abc.testspace.com' \
              '/api/projects/project1/spaces/space1/result_sets'
        mock.get(url, text=request_text)
        response = testspace_rest_query.space_result_sets('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                          'abc',
                                                          'project1',
                                                          'space1',
                                                          enterprise=None)
        self.assertEqual(response.status_code, 200)

    @requests_mock.mock()
    def test_space_result_sets_enterprise(self, mock):
        url = 'https://ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS:@abc.testspace.xyz.com/' \
              'api/projects/project1/spaces/space1/result_sets'
        mock.get(url, text=request_text)
        response = testspace_rest_query.space_result_sets('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                          'abc',
                                                          'project1',
                                                          'space1',
                                                          enterprise='xyz')
        self.assertEqual(response.status_code, 200)

    @requests_mock.mock()
    def test_space_result_set_enterprise(self, mock):
        url = 'https://ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS:@abc.testspace.xyz.com/' \
              'api/projects/project1/spaces/space1/result_sets/8712'
        mock.get(url, text=request_text)
        response = testspace_rest_query.space_result_set('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                         'abc',
                                                         'project1',
                                                         'space1',
                                                         8712,
                                                         enterprise='xyz')
        self.assertEqual(response.status_code, 200)
        result_set = result_set_api.TestspaceResultSetApi(response.json())
        self.assertEqual(int(request_url.split('/')[-1]), result_set.get_result_id())


class TestResultApi(TestCase):
    @requests_mock.mock()
    def test_result_id(self, mock):
        mock.get(request_url, text=request_text)
        result_response = testspace_rest_query.testspace_get(request_url)
        if result_response.status_code is 200:
            result_set = result_set_api.TestspaceResultSetApi(result_response.json())
            self.assertEqual(int(request_url.split('/')[-1]), result_set.get_result_id())

    @requests_mock.mock()
    def test_number_passing(self, mock):
        mock.get(request_url, text=request_text)
        result_response = testspace_rest_query.testspace_get(request_url)
        result_set = result_set_api.TestspaceResultSetApi(result_response.json())
        passing = result_set.get_passing_tests()
        self.assertEqual(42, passing)

    @requests_mock.mock()
    def test_number_failing(self, mock):
        mock.get(request_url, text=request_text)
        result_response = testspace_rest_query.testspace_get(request_url)
        result_set = result_set_api.TestspaceResultSetApi(result_response.json())
        self.assertEqual(0, result_set.get_failing_tests())

    @requests_mock.mock()
    def test_health_state_healthy(self, mock):
        mock.get(request_url, text=request_text)
        result_response = testspace_rest_query.testspace_get(request_url)
        result_set = result_set_api.TestspaceResultSetApi(result_response.json())
        self.assertEqual('success', result_set.get_health_state())

    @requests_mock.mock()
    def test_health_description_healthy(self, mock):
        mock.get(request_url, text=request_text)
        result_response = testspace_rest_query.testspace_get(request_url)
        result_set = result_set_api.TestspaceResultSetApi(result_response.json())
        self.assertIn('Healthy', result_set.get_health_description())

    @requests_mock.mock()
    def test_number_excluded_test_cases(self, mock):
        mock.get(request_url, text=request_text)
        result_response = testspace_rest_query.testspace_get(request_url)
        result_set = result_set_api.TestspaceResultSetApi(result_response.json())
        self.assertEqual(3, result_set.get_number_excluded_test_cases())

    @requests_mock.mock()
    def test_list_excluded_test_cases(self, mock):
        mock.get(request_url, text=request_text)
        result_response = testspace_rest_query.testspace_get(request_url)
        result_set = result_set_api.TestspaceResultSetApi(result_response.json())
        self.assertEqual(3, len(result_set.get_excluded_tests()))
