from unittest import TestCase
import requests_mock
from api import project_api
from api import testspace_rest_query
import json


with open("projects.txt") as myfile:
    request_text = "".join(line.rstrip() for line in myfile)

request_json = json.loads(request_text)
request_url = 'https://ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS:@abc.testspace.com/api/projects'


class TestSpaceProjectApi(TestCase):
    @requests_mock.mock()
    def test_project_list(self, mock):
        mock.get(request_url, text=request_text)
        response = testspace_rest_query.projects_get('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                     'abc',
                                                     enterprise=None)
        results_sets = project_api.TestspaceProjectApi(response.json())
        project_list = results_sets.get_project_list()
        self.assertTrue("testspace-samples:cpp.cpputest" in project_list)

    @requests_mock.mock()
    def test_project_name_by_id(self, mock):
        mock.get(request_url, text=request_text)
        response = testspace_rest_query.projects_get('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                     'abc',
                                                     enterprise=None)
        results_sets = project_api.TestspaceProjectApi(response.json())
        self.assertEqual('testspace-samples:csharp.nunit', results_sets.get_project_name_by_id(168))

    @requests_mock.mock()
    def test_project_name_by_invalid_id(self, mock):
        mock.get(request_url, text=request_text)
        response = testspace_rest_query.projects_get('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                     'abc',
                                                     enterprise=None)
        results_sets = project_api.TestspaceProjectApi(response.json())
        self.assertEqual(None, results_sets.get_project_name_by_id(100))

    @requests_mock.mock()
    def test_project_id_by_name(self, mock):
        mock.get(request_url, text=request_text)
        response = testspace_rest_query.projects_get('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                     'abc',
                                                     enterprise=None)
        results_sets = project_api.TestspaceProjectApi(response.json())
        self.assertEqual(158, results_sets.get_project_id_by_name('testspace-samples:ruby.rspec'))

    @requests_mock.mock()
    def test_project_id_by_invalid_name(self, mock):
        mock.get(request_url, text=request_text)
        response = testspace_rest_query.projects_get('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                     'abc',
                                                     enterprise=None)
        results_sets = project_api.TestspaceProjectApi(response.json())
        self.assertEqual(None, results_sets.get_project_id_by_name('invalid_project_name'))
